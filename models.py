from sqlalchemy import Column, ForeignKey, Integer, String, Boolean
from database import Base

class Avatars(Base):
    __tablename__ = 'avatars'
    id = Column(Integer, primary_key=True)
    pnut_user = Column(String(250), unique=True)
    avatar = Column(String(250))

class Rooms(Base):
    __tablename__ = 'rooms'
    id = Column(Integer, primary_key=True)
    room_id = Column(String(250), unique=True)
    pnut_chan = Column(Integer, unique=True)
    portal = Column(Boolean)

class DirectRooms(Base):
    __tablename__ = 'direct'
    id = Column(Integer, primary_key=True)
    room_id = Column(String(250), unique=True)
    pnut_chan = Column(Integer, unique=True)
    bridge_user = Column(String(250))

class ControlRooms(Base):
    __tablename__ = 'control'
    id = Column(Integer, primary_key=True)
    room_id = Column(String(250), unique=True)

class Events(Base):
    __tablename__ = 'events'
    id = Column(Integer, primary_key=True)
    event_id = Column(String(250))
    room_id = Column(String(250))
    pnut_msg_id = Column(Integer)
    pnut_user_id = Column(Integer)
    pnut_chan_id = Column(Integer)
    deleted = Column(Boolean)

class Users(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    matrix_id = Column(String(250))
    pnut_user_id = Column(Integer)
    pnut_user_token = Column(String(250))
