import logging
import yaml
import time
import pnutpy

from matrix_bot_api.matrix_bot_api import MatrixBotAPI
from matrix_bot_api.mregex_handler import MRegexHandler
from matrix_bot_api.mcommand_handler import MCommandHandler
from models import Avatars, Rooms, Events, Users
from database import db_session
from sqlalchemy import and_


def help_cb(room, event):
    reply = "This is an admin room for controlling your connection to pnut.io\n"
    reply += "The following commands are available.\n\n"
    reply += "!auth -  Authorize your account on pnut.io\n"
    reply += "!save <token> -  Save your pnut.io auth token\n"
    reply += "!drop -  Drop your pnut.io auth token\n"
    reply += "!status -  Status of your pnut.io auth token\n"
    room.send_notice(reply)

def auth_cb(room, event):
    reply = "Visit the following URL to authorize your account on pnut.io.\n\n"
    reply += "https://pnut.io/oauth/authenticate"
    reply += "?client_id=6SeCRCpCZkmZOKFLFGWbcdAeq2fX1M5t"
    reply += "&redirect_uri=urn:ietf:wg:oauth:2.0:oob"
    reply += "&scope=write_post,presence,messages&response_type=token\n\n"
    reply += "You will be provided with a token that you store with the !save command.\n"
    room.send_notice(reply)

def save_cb(room, event):
    args = event['content']['body'].split(' ', maxsplit=1)

    if len(args) < 2:
        reply = "You must provide a token with this command.\n"
        reply += "!save <token>"
        room.send_notice(reply)
        return

    pnutpy.api.add_authorization_token(args[1])
    try:
        response, meta = pnutpy.api.get_user('me')

        user = Users(
            matrix_id=event['sender'],
            pnut_user_id=response.id,
            pnut_user_token=args[1]
        )
        db_session.add(user)
        db_session.commit()

        reply = "Success! You are now authorized as " + response['username']

    except pnutpy.errors.PnutAuthAPIException as e:
        reply = "Error!  Unable to authorize your account."

    except Exception as e:
        logging.exception('!save')
        reply = "Error!  Problem saving your token."

    room.send_notice(reply)

def drop_cb(room, event):
    try:
        user = Users.query.filter(Users.matrix_id == event['sender']).one_or_none()
        if user is not None:
            db_session.delete(user)
            db_session.commit()
            reply = "Success!  Your auth token has been removed."
        else:
            reply = "You do not appear to be registered."       

    except Exception as e:
        logging.exception('!drop')
        reply = "Error!  Problem removing your token."

    room.send_notice(reply)

def status_cb(room, event):
    try:
        user = Users.query.filter(Users.matrix_id == event['sender']).one_or_none()
        if user is None:
            reply = "You are currently not authorized on pnut.io"
        else:
            pnutpy.api.add_authorization_token(user.pnut_user_token)
            response, meta = pnutpy.api.get_user('me')
            reply = "You are currently authorized as " + response.username

    except pnutpy.errors.PnutAuthAPIException as e:
        reply = "You are currently not authorized on pnut.io"

    except Exception as e:
        logging.exception('!status')
        reply = "Error!  There was a problem checking your account."

    room.send_notice(reply)


if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)

    with open("config.yaml", "rb") as config_file:
        config = yaml.load(config_file)

    bot = MatrixBotAPI(config['TBOT_USER'], config['TBOT_PASS'], config['MATRIX_HOST'])

    bot.add_handler(MCommandHandler("help", help_cb))
    bot.add_handler(MCommandHandler("auth", auth_cb))
    bot.add_handler(MCommandHandler("save", save_cb))
    bot.add_handler(MCommandHandler("drop", drop_cb))
    bot.add_handler(MCommandHandler("status", status_cb))

    bot.start_polling()

    while True:
        time.sleep(1)
