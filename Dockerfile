FROM python:3.11-slim-bookworm

WORKDIR /usr/src/app

RUN apt-get update && apt-get install libmagic-dev curl -y

COPY . .
RUN pip install --no-cache-dir -r requirements.txt

ENV CONFIG_FILE=/data/config.yaml
VOLUME /data
WORKDIR /data
EXPOSE 5000
CMD [ "python", "/usr/src/app/pnut-matrix.py" ]
